package service

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

// GetBookOnGoogle queries the google API and returns the books or a error
func GetBookOnGoogle(term, skip, limit string) (map[string]interface{}, error) {
	if strings.TrimSpace(skip) == "" {
		skip = "0"
	}

	if strings.TrimSpace(limit) == "" {
		limit = "10"
	}

	resp, err := http.Get(fmt.Sprintf("https://www.googleapis.com/books/v1/volumes?q=%s&maxResults=%s&startIndex=%s", term, limit, skip))
	if err != nil {
		return nil, err
	}

	if resp.StatusCode <= 200 && resp.StatusCode >= 300 {
		return nil, fmt.Errorf("invalid returno from Google API")
	}

	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var m map[string]interface{}
	err = json.Unmarshal(data, &m)
	return m, err
}
