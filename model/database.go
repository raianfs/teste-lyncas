package model

import "fmt"

var favoriteBooksIds []string

func init() {
	favoriteBooksIds = []string{}
}

func isBookAlreadyFavorited(id string) bool {
	for _, v := range favoriteBooksIds {
		if v == id {
			return true
		}
	}
	return false
}

// AddFavoriteBook adds a book to the favorite list
func AddFavoriteBook(id string) error {
	if isBookAlreadyFavorited(id) {
		return fmt.Errorf("book is already on the list")
	}
	favoriteBooksIds = append(favoriteBooksIds, id)
	return nil
}

// RemoveFavoriteBook remove a book from the favorite list
func RemoveFavoriteBook(id string) error {
	var removed = false
	var f []string
	for _, v := range favoriteBooksIds {
		if id != v {
			f = append(f, v)
		} else {
			removed = true
		}
	}
	if removed {
		favoriteBooksIds = f
		return nil
	}
	return fmt.Errorf("book is not on the list")
}

// GetFavoriteBooks get all favorite books
func GetFavoriteBooks() []string {
	return favoriteBooksIds
}
