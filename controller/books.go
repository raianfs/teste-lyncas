package controller

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/teste-lyncas/model"
	"gitlab.com/teste-lyncas/service"
)

// GetBooks get all books based on a pattern
func GetBooks(c *gin.Context) {
	skip := strings.TrimSpace(c.Query("skip"))
	limit := strings.TrimSpace(c.Query("limit"))
	term := strings.TrimSpace(c.Query("term"))

	books, err := service.GetBookOnGoogle(term, skip, limit)
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, fmt.Errorf("invalid return from server"))
		return
	}

	c.JSON(http.StatusOK, books)
}

// PostFavoriteBook insert a book in the favorite list
func PostFavoriteBook(c *gin.Context) {
	id := strings.TrimSpace(c.Param("id"))
	if id == "" {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("invalid id"))
		return
	}

	err := model.AddFavoriteBook(id)
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	c.JSON(http.StatusNoContent, nil)
}

// GetFavoritesBook return all list of favorite books
func GetFavoritesBook(c *gin.Context) {
	books := model.GetFavoriteBooks()
	c.JSON(http.StatusOK, books)
}

// DeleteFavoriteBook delete a book from favorite list
func DeleteFavoriteBook(c *gin.Context) {
	id := strings.TrimSpace(c.Param("id"))
	if id == "" {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("invalid id"))
		return
	}

	err := model.RemoveFavoriteBook(id)
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	c.JSON(http.StatusNoContent, nil)
}
